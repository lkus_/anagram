﻿using System;
using System.Linq;

namespace Anagram
{
    public class WordWithAnagramKey
    {
        public string Word { get; set; }
        public string AnagramKey { get; set; }

        public WordWithAnagramKey(string word)
        {
            Word = word;
            AnagramKey = GetOrderedCharacters(word);
        }

        private string GetOrderedCharacters(string word)
        {
            return String.Concat(word.OrderBy(x => x));
        }
    }
}
