﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anagram.Models
{
    public class GroupedAnagrams
    {
        public List<string> Anagrams { get; set; }
    }
}
