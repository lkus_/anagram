﻿using Anagram.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Anagram
{
    public static class AnagramManager
    {
        static List<GroupedAnagrams> groupedAnagrams = new List<GroupedAnagrams>();

        public static List<GroupedAnagrams> SearchAnagrams(List<string> words)
        {
            List<WordWithAnagramKey> wordsWithAnagramKey = LoadWordWithAnagramKey(words);

            return GroupWordsIntoAnagrams(wordsWithAnagramKey);
        }
        public static List<GroupedAnagrams> FindTheLongesWordsAreAnagrams()
        {
            var longestWordCharsCount = groupedAnagrams.Max(x => x.Anagrams[0].Length);
            if (AreAnyAnagrams())
            {
                return groupedAnagrams.Where(x => x.Anagrams[0].Length == longestWordCharsCount).ToList();
            }

            return null;
        }

        public static List<GroupedAnagrams> FindLongestSetOfAnagrams()
        {
            var maxCount = groupedAnagrams.Max(x => x.Anagrams.Count);

            if (AreAnyAnagrams())
            {
                return groupedAnagrams.Where(x => x.Anagrams.Count == maxCount).ToList();
            }

            return null;
        }

        public static string FormatDataToOutput()
        {
            StringBuilder output = new StringBuilder();

            var writeGroupedAnagrams = DocumentHelper.WriteAnagrams(groupedAnagrams);
            output.Append(writeGroupedAnagrams);

            output.AppendLine().Append("Longest set of anagrams:").AppendLine();
            var longestSetOfAnagrams = FindLongestSetOfAnagrams();
            var writeLongestSetOfAnagrams = DocumentHelper.WriteAnagrams(longestSetOfAnagrams);
            output.Append(writeLongestSetOfAnagrams);

            output.AppendLine().Append("Longest words are anagrams:").AppendLine();
            var longesWordsAreAnagrams = FindTheLongesWordsAreAnagrams();
            var writeLongesWordsAreAnagrams = DocumentHelper.WriteAnagrams(longesWordsAreAnagrams);
            output.Append(writeLongesWordsAreAnagrams);

            return output.ToString();
        }

        private static List<WordWithAnagramKey> LoadWordWithAnagramKey(List<string> words)
        {
            List<WordWithAnagramKey> wordsWithAnagramKey = new List<WordWithAnagramKey>();
            foreach (string word in words)
            {
                var anagram = new WordWithAnagramKey(word);
                wordsWithAnagramKey.Add(anagram);
            }

            return wordsWithAnagramKey;
        }

        private static List<GroupedAnagrams> GroupWordsIntoAnagrams(List<WordWithAnagramKey> wordsWithAnagramKey)
        {
            groupedAnagrams = wordsWithAnagramKey.GroupBy(x => x.AnagramKey).Select(x => new GroupedAnagrams
            {
                Anagrams = x.Select(y => y.Word).ToList()
            }).ToList();

            return groupedAnagrams;
        }

        private static bool AreAnyAnagrams()
        {
            var maxCount = groupedAnagrams.Max(x => x.Anagrams.Count);
            if (maxCount <= 1)
            {
                return false;
            }

            return true;
        }
    }
}
