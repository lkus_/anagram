﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anagram
{
    public static class PathHelper
    {
        const string testFileName = "test.txt";
        const string outputFileName = "output.txt";

        static string fileDirectory = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\"));
        static string filePath;
        public static string GetFilePath(string providedPath)
        {
            if (providedPath == "")
            {
                filePath = fileDirectory;
                return Path.Combine(fileDirectory, testFileName);
            }
            filePath = providedPath;
            return providedPath;
        }

        public static string GetOutputFileDirectory()
        {
            return Path.Combine(Path.GetDirectoryName(filePath), outputFileName);
        }
    }
}
