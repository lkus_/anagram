﻿using Anagram.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anagram
{
    public static class DocumentHelper
    {
        public static string WriteAnagrams(List<GroupedAnagrams> groupedAnagrams)
        {
            StringBuilder output = new StringBuilder();

            foreach (var groupedAnagram in groupedAnagrams)
            {
                foreach (var anagram in groupedAnagram.Anagrams)
                {
                    output.Append(anagram).Append(" ");
                }
                output.AppendLine();
            }

            return output.ToString();
        }
    }
}
