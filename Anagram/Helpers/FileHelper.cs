﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Anagram
{
    public static class FileHelper
    {
        public static List<string> ReadFile(string path)
        {
            try
            {
                List<string> output = new List<string>();
                foreach (string line in File.ReadLines(path))
                {
                    output.Add(line);
                }
                return output;
            }
            catch (FileNotFoundException exnotfound)
            {
                Console.WriteLine("Cannot find file");
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public static void SaveFile(string path, string fileContent)
        {
            File.WriteAllText(path, fileContent);
        }
    }
}
