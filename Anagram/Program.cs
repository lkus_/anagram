﻿using System;
using System.IO;

namespace Anagram
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please provide path to the file or leave it empty (press Enter) to use default file");

            string providedPath = Console.ReadLine();

            var inputFilePath = PathHelper.GetFilePath(providedPath);
            var outputFilePath = PathHelper.GetOutputFileDirectory();
            
            var inputFileContent = FileHelper.ReadFile(inputFilePath);

            if(inputFileContent != null)
            {
                AnagramManager.SearchAnagrams(inputFileContent);
                var outputFileContent = AnagramManager.FormatDataToOutput();
                FileHelper.SaveFile(outputFilePath, outputFileContent);

                Console.WriteLine("Please check {0} file.", outputFilePath);
            }
            
            Console.ReadKey();
        }
    }
}
