﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Anagram.Tests
{
    [TestClass]
    public class AnagramTests
    {
        List<string> testDataWithAnagrams = new List<string>
        {
            "szukal", "masa", "lol", "lukasz", "sama", "luka", "kula"
        };

        List<string> testDataWithNoAnagrams = new List<string>
        {
            "aaa", "bbb", "cccc", "dddd"
        };

        [TestMethod]
        public void Search_Anagrams_Exists()
        {
            var actual = AnagramManager.SearchAnagrams(testDataWithAnagrams);

            Assert.AreEqual(4, actual.Count);
        }

        [TestMethod]
        public void Search_Anagrams_NotExists()
        {
            var actual = AnagramManager.SearchAnagrams(testDataWithNoAnagrams);

            Assert.AreEqual(4, actual.Count);
        }

        [TestMethod]
        public void Longest_Set_Anagrams_Exists()
        {
            AnagramManager.SearchAnagrams(testDataWithAnagrams);
            var actual = AnagramManager.FindLongestSetOfAnagrams();

            Assert.AreEqual(3, actual.Count);
            Assert.AreEqual(2, actual[0].Anagrams.Count);
        }

        [TestMethod]
        public void Longest_Set_Anagrams_NotExists()
        {
            AnagramManager.SearchAnagrams(testDataWithNoAnagrams);
            var actual = AnagramManager.FindLongestSetOfAnagrams();

            Assert.AreEqual(null, actual);
        }

        [TestMethod]
        public void Longest_Words_Are_Anagrams_Exists()
        {
            AnagramManager.SearchAnagrams(testDataWithAnagrams);
            var actual = AnagramManager.FindTheLongesWordsAreAnagrams();

            Assert.AreEqual(1, actual.Count);
        }

        [TestMethod]
        public void Longest_Words_Are_Anagrams_NotExists()
        {
            AnagramManager.SearchAnagrams(testDataWithNoAnagrams);
            var actual = AnagramManager.FindLongestSetOfAnagrams();

            Assert.AreEqual(null, actual);
        }
    }
}
